const inputOptionsPromise = new Promise(function(resolve) {
    // get your data and pass it to resolve()
    setTimeout(function() {

        $.getJSON("https://trial.mobiscroll.com/content/countries.json", function(data) {
            resolve(data)
            console.log(data);
        });

    }, 2000)
})
$(function() {
    $("#taginfo").click(function() {
        console.log("click on tag info");
        Swal.fire({
            title: 'Select Tag',
            input: 'select',
            inputOptions: inputOptionsPromise,
            inputPlaceholder: 'Select tag',
            showCancelButton: true,
            inputValidator: function(value) {
                return new Promise(function(resolve, reject) {
                    if (value !== '') {
                        document.getElementById('taginfo').value = value;
                        resolve();
                    } else {
                        reject('You need to select one tag')
                    }
                })
            }
        }).then(function(result) {
            Swal.fire({
                type: 'success',
                html: 'You selected: ' + result
            })
        })
    });
});